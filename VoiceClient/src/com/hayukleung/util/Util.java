package com.hayukleung.util;

import java.io.File;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class Util
{
  private final static String TAG = "Util";

  /*
   * check if sd card exists if exists return true else false
   */
  public static boolean hasSDCard()
  {
    if (!Environment.MEDIA_MOUNTED
        .equals(Environment.getExternalStorageState()))
    {
      Log.e(TAG, "SDCard is unavailable ...");
      return false;
    }
    else
    {
      Log.i(TAG, "SDCard is available ...");
      return true;
    }
  }

  /*
   * if directory exists, return without nothing to do else build the directory
   * 
   * @param path return void
   */
  public static void mkdirIfNotExist(final String path)
  {
    File file = new File(path);
    if (file.exists())
    {
      return;
    }
    else
    {
      file.mkdirs();
    }
  }

  /*
   * show toast
   * 
   * @param context, message return void
   */
  public static void showToast(Context context, final String message)
  {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
  }
}

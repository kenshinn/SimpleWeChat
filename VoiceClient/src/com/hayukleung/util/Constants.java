package com.hayukleung.util;

import android.os.Environment;

public class Constants
{
	public final static String APP_NAME = "VoiceClient";
	public final static String ROOT = Environment.getExternalStorageDirectory().getAbsolutePath();
	public final static String BASE_DIR = ROOT + "/" + APP_NAME;
	// IP
	public final static String SERVER_IPV4 = "192.168.31.56";
	// TCP
	public final static String SERVER_TCP_PORT = "8081";
	public final static int SERVER_TCP_PORT_INT = Integer.parseInt(SERVER_TCP_PORT);
	// UDP
	//public final static String SERVER_UDP_PORT = "1234";
	//public final static int SERVER_UDP_PORT_INT = Integer.parseInt(SERVER_UDP_PORT);
	public final static String CLIENT_UDP_PORT = "7777";
	public final static int CLIENT_UDP_PORT_INT = Integer.parseInt(CLIENT_UDP_PORT);
	// REPLY LABEL
	public final static String REPLY_OK = "ok";
	public final static String REPLY_ERROR = "error";
	// REQUEST LABEL
	public final static String REQ_LOGIN = "login";
	public final static String REQ_DOWNLOAD = "get";
	public final static String REQ_UPLOAD = "upload";
	//
	public final static String COMMA = ",";
	// 1K
    public final static int K = 1024;
}

package com.hayukleung.base;

public interface DoOnCreate
{
	void initWidgets();
	void setContentView();
}

package com.hayukleung.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

public class BaseActivity extends Activity implements DoOnCreate
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView();
		initWidgets();
	}

	@Override
  public void initWidgets()
  {
	  // TODO initialize widgets here
  }

	@Override
  public void setContentView()
  {
	  // TODO set content view here
  }
	
	
}
